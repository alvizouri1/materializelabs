<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected static function boot() {
        parent::boot();

        static::deleted(function ($author) {
            $author->books()->delete();
        });
    }

    public function books()
    {
        return $this->belongsTo('App\Models\Book');
    }
}
