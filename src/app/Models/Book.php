<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $appends = ['authors_full_name'];

    public function author()
    {
        return $this->belongsTo('App\Models\Author', 'author_id', 'id');
    }

    /**
     * Get the author's full name.
     *
     * @param  string  $value
     * @return string
     */
    public function getAuthorsFullNameAttribute()
    {
        $author = $this->author;
        return $author->first_name . " " . $author->last_name;
    }
}
