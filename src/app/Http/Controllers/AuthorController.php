<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreOrUpdateAuthor;

// Models
use App\Models\Author;

// Resources
use App\Http\Resources\Author as AuthorResource;
use App\Http\Resources\Authors as AuthorsCollection;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('paginated')) {
            $sortField = $request->get('sortField', null);
            $sortOrder = $request->get('sortOrder', null);
            $size = $request->get('size', 10);
    
            if ($sortField && $sortOrder) {
                $authors = Author::orderBy($sortField, $sortOrder == "ascend" ? "asc" : "desc")->paginate($size);
            } else {
                $authors = Author::paginate($size);
            }

            return new AuthorsCollection($authors);
        }
        else
            return new AuthorsCollection(Author::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateAuthor $request)
    {
        $r = $request->validated();
        $already = Author::whereRaw("LOWER(first_name) = '" . $r['first_name'] . "' AND LOWER(last_name) = '" . $r['last_name'] . "'")->first();
        if ($already) {
            return response()->json(['message' => __('messages.author.validation.already_exists', ['full_name' => $r['first_name'] . " " . $r['last_name']])], 400);
        }

        $author = new Author;
        $author->first_name = $r['first_name'];
        $author->last_name = $r['last_name'];
        $author->save();
        return response()->json(['success' => true, 'id' => $author->id, 'message' => __('messages.author.store.success', ['full_name' => $author->first_name . " " . $author->last_name])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new AuthorResource(Author::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrUpdateAuthor $request, $id)
    {
        $r = $request->validated();
        $already = Author::whereRaw("id != $id AND LOWER(first_name) = '" . $r['first_name'] . "' AND LOWER(last_name) = '" . $r['last_name'] . "'")->first();
        if ($already) {
            return response()->json(['message' => __('messages.author.validation.already_exists', ['full_name' => $r['first_name'] . " " . $r['last_name']])], 400);
        }
    
        $author = Author::findOrFail($id);
        $author->first_name = $r['first_name'];
        $author->last_name = $r['last_name'];
        $author->save();
        return response()->json(['success' => true, 'message' => __('messages.author.update.success', ['full_name' => $author->first_name . " " . $author->last_name])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);

        if ($author) {
            $_message = __('messages.author.destroy.success', ['full_name' => $author->first_name . ' ' . $author->last_name]);
            $author->delete();
        } else
            $_message = __('messages.general.meh.deletion', ['id' => $id]);

        return response()->json(['success' => true, 'message' => $_message]);
    }
}
