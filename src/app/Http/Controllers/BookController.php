<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreOrUpdateBook;

// Models
use App\Models\Book;

// Resources
use App\Http\Resources\Book as BookResource;
use App\Http\Resources\Books as BooksCollection;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortField = $request->get('sortField', null);
        $sortOrder = $request->get('sortOrder', null);
        $size = $request->get('size', 10);

        if ($sortField && $sortOrder) {
            $books = Book::orderBy($sortField, $sortOrder == "ascend" ? "asc" : "desc")->paginate($size);
        } else {
            $books = Book::paginate($size);
        }

        return new BooksCollection($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateBook $request)
    {
        $r = $request->validated();
        $book = new Book;
        $book->title = $r['title'];
        $book->date_published = $r['date_published'];
        $book->author_id = $r['author_id'];
        $book->isbn = $r['isbn'];
        $book->save();
        return response()->json(['success' => true, 'id' => $book->id, 'message' => __('messages.author.store.success', ['title' => $book->title])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BookResource(Book::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrUpdateBook $request, $id)
    {
        $r = $request->validated();
        $book = Book::findOrFail($id);
        $book->title = $r['title'];
        $book->date_published = $r['date_published'];
        $book->author_id = $r['author_id'];
        $book->isbn = $r['isbn'];
        $book->save();
        return response()->json(['success' => true, 'message' => __('messages.book.update.success', ['title' => $book->title])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if ($book) {
            $_message = __('messages.book.destroy.success', ['title' => $book->title]);
            $book->delete();
        } else
            $_message = __('messages.general.meh.deletion', ['id' => $id]);

        return response()->json(['success' => true, 'message' => $_message]);
    }
}
