<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOrUpdateBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required|string|max:25',
            'date_published'    => 'required|date',
            'author_id'         => 'required|exists:authors,id',
            'isbn'              => [
                'required',
                'integer',
                'regex:/^[0-9]{13}$/',
                 $this->book ? Rule::unique('books', 'isbn')->ignore($this->book) : 'unique:books,isbn'
            ],
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'isbn.regex' => 'The isbn format is invalid. It must be 13 characters.',
        ];
    }
}
