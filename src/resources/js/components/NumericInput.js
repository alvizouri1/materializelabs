import React, { Component } from "react";
import { Input } from "antd";
import "./index.css";

class NumericInput extends Component {
  inputRef = React.createRef();
  onChange = (e) => {
    const { value } = e.target;
    const reg = /^-?\d*(\.\d*)?$/;
    if (((!isNaN(value) && reg.test(value)) || value === "" || value === "-") && this.props.onChange) {
      this.props.onChange(value);
    }
  };

  // '.' at the end or only '-' in the input box.
  onBlur = () => {
    if (this.inputRef && this.inputRef.current) {
        const { onBlur, onChange } = this.props;
        const value = this.inputRef.current.props.value.toString();
        let valueTemp = value;
        if (value.charAt(value.length - 1) === "." || value === "-") {
            valueTemp = value.slice(0, -1);
        }
        if (onChange) {
            onChange(valueTemp.replace(/0*(\d+)/, "$1"));
        }
        if (onBlur) {
            onBlur();
        }
    }
  };

  render() {
    return (
        <Input
          {...this.props}
          onChange={this.onChange}
          onBlur={this.onBlur}
          placeholder="Input a number"
          maxLength={this.props.maxLength}
          ref={this.inputRef}
        />
    );
  }
}

export default NumericInput;