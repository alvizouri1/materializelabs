import React from "react";
import { 
    Link,
    withRouter
} from "react-router-dom";
import { Menu } from 'antd';
import {
    UserOutlined,
    VideoCameraOutlined,
  } from '@ant-design/icons';

const CustomMenu = ({location}) => {
    let active = location.pathname.substring(1);
    active = active == "" ? "books" : active;
    return (<Menu theme="dark" mode="inline" defaultSelectedKeys={[active]}>
    <Menu.Item key="books" icon={<UserOutlined />}>
      <Link to="/">Books</Link>
    </Menu.Item>
    <Menu.Item key="authors" icon={<VideoCameraOutlined />}>
      <Link to="/authors">Authors</Link>
    </Menu.Item>
  </Menu>);
}

export default withRouter(CustomMenu);