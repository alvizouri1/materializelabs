import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

// UI
import { Layout } from 'antd';
import Menu from '../../components/Menu';
import 'antd/dist/antd.css'; 
import './style.css';

import AuthorList from '../../pages/author/List';
import BookList from '../../pages/book/List';

const { Header, Sider, Content, Footer } = Layout;

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
        collapsed: true,
      };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  componentDidUpdate() {
  }

  render() {
    return (
      <Layout className="auth-layout">
        <Router>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {
            // console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            // console.log(collapsed, type);
          }}
        >
            <div className="logo" />
            <Menu />
          </Sider>
          <Layout>
            <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
            <Content style={{ margin: '24px 16px 0' }}>
              <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                <Switch>
                  <Route path="/authors">
                    <AuthorList />
                  </Route>
                  <Route path="/">
                    <BookList />
                  </Route>
                </Switch>
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
          </Layout>
        </Router>
      </Layout>
    );
  }
}

export default Auth;

if (document.getElementById('app')) {
    ReactDOM.render(<Auth />, document.getElementById('app'));
}