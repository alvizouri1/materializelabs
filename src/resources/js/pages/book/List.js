import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Button, Drawer, Popconfirm, Skeleton, Space, message, responsiveObserver } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import axios from '../../axios';
import BookForm from './BookForm';

class List extends Component {
  state = {
    data: [],
    tableParams: {
      sortField: null,
      sortOrder: null,
      pagination: {
        current: 1,
        pageSize: 10,
        total: 200
      },
    },
    drawer: {
      title: "",
      visible: false,
      validForm: true,
      formIsSubmitting: false,
    },
    authors: null,
    loading: false,
    isFormSubmitting: false,
    formConfig: {
      editData: null,
      url: 'books',
      method: 'post'
    },
    formEditData: null,
    formUrl: 'books',
  };

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      sorter: true,
      responsive: ['md'],
    },
    {
      title: 'Title',
      dataIndex: 'title',
    },
    {
      title: 'Author',
      dataIndex: 'authors_full_name',
    },
    {
      title: 'Date Published',
      dataIndex: 'date_published',
      responsive: ['md'],
    },
    {
      title: 'ISBN',
      dataIndex: 'isbn',
      responsive: ['md'],
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => {
        return (<>
            <Button type="link" onClick={() => {
              this.showDrawer(`Editing book with id "${record.id}"`, {
                id: record.id,
                title: record.title,
                date_published: record.date_published,
                author_id: record.author_id,
                isbn: record.isbn
              });
            }}><EditOutlined /></Button>
            <Popconfirm
              placement="leftBottom"
              title="Are you sure to delete this book?"
              onConfirm={() => {this.confirm(record.id);}}
              okText="Yes"
              cancelText="No"
            >
              <Button type="link"><DeleteOutlined /></Button>
            </Popconfirm>
        </>);
      },
    },
  ];

  formRef = React.createRef();

  confirm(id) {
    axios.delete(`books/${id}`)
    .then(({data}) => {
        if (data.success) {
          message.info(data.message);
          this.reload(true);
        }
    });
  }

  componentDidMount() {
    const { tableParams } = this.state;
    this.fetch(tableParams);
  }
  
  componentDidUpdate(prevProps, prevState) {
    if (prevState.formConfig != this.state.formConfig && this.formRef && this.formRef.current) {
      this.formRef.current.resetFields();
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    this.fetch({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination,
      ...filters,
    });
  };

  fetch = ({sortField, sortOrder, pagination}) => {
    this.setState({ loading: true });
    axios.get('books', {  
        params: {
          page: pagination.current,
          size: pagination.pageSize,
          sortField,
          sortOrder
        }
     })
    .then((res) => {
        const {data, meta} = res.data;
        this.setState({
          loading: false,
          data: data,
          tableParams: {
            sortField,
            sortOrder,
            pagination: {
              current: meta.current_page,
              pageSize: meta.per_page,
              total: meta.total,
            },
          },
        });
    });
  };

  showDrawer = (title, formEditData = null) => {
    const drawer = {...this.state.drawer};
    const formConfig = {url: 'books/', editData: formEditData};
    drawer.title = title;
    drawer.visible = true;

    if (formEditData) {
      formConfig.url += formEditData.id;
      formConfig.method = 'put';
    }
    else {
      formConfig.method = 'post';
    }

    this.setState({drawer, formEditData, formConfig});
    axios.get('authors')
    .then(({data}) => {
        this.setState({
          authors: data.data,
        });
    });
  };

  onClose = (reload = null) => {
    if (reload) {
      this.reload(reload);
    }

    const drawer = {...this.state.drawer};
    drawer.visible = false;
    this.setState({drawer});
  };

  reload = (reload = null) => {
    const tableParams = {...this.state.tableParams};
    if (reload.sortField) tableParams.sortField = reload.sortField;
    if (reload.sortOrder) tableParams.sortOrder = reload.sortOrder;
    tableParams.pagination.page = 1;
    this.fetch(tableParams);
  } 

  setValidForm = (valid) => {
    const drawer = {...this.state.drawer};
    drawer.validForm = valid;
    this.setState({drawer});
  }

  setIsFormSubmitting = (value) => {
    this.setState({isFormSubmitting: value});
  }

  render() {
    const { authors, data, tableParams, drawer, loading, formConfig, isFormSubmitting } = this.state;

    return (<Space direction="vertical">
          <Button type="primary" onClick={() => this.showDrawer("Add a new book")}>Add new book</Button>
          <Drawer
          title={drawer.title}
          width={300}
          onClose={() => {this.onClose()}}
          visible={drawer.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: 'right',
              }}
            >
              <Button onClick={() => {this.onClose()}} style={{ marginRight: 8 }} disabled={isFormSubmitting}>
                Cancel
              </Button>
              <Button onClick={() => this.formRef.current.submit()} htmlType="submit" type="primary" loading={isFormSubmitting}>
                Submit
              </Button>
            </div>
          }
        >
          {drawer.visible && authors ? <BookForm formConfig={formConfig} authors={authors} setValidForm={this.setValidForm} formRef={this.formRef} closeDrawer={this.onClose} setIsFormSubmitting={this.setIsFormSubmitting} /> : <Skeleton active />}
        </Drawer>
          <Table
            columns={this.columns}
            rowKey={record => record.id}
            dataSource={data}
            pagination={tableParams.pagination}
            loading={loading}
            onChange={this.handleTableChange}
          />
    </Space>);
  }
}

List.propTypes = {

}

export default List