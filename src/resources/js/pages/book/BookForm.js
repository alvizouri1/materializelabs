import React, { useState } from 'react'
import { Alert, Form, Col, Row, Input, Select, DatePicker } from 'antd';
import moment from "moment";
import NumericInput from '../../components/NumericInput';
import axios from "../../axios";

const BookForm = ({
  authors,
  formRef,
  closeDrawer,
  setIsFormSubmitting,
  formConfig: {
    editData = {
      id: null,
      title: null,
      date_published: null,
      author_id: null,
      isbn: null
    },
    url,
    method
  },
}) => {
  const formattedFormData = {...editData};
  const dateFormat = "YYYY-MM-DD";
  
  const [error, setError] = useState(null);

  if (formattedFormData.date_published != null) {
    formattedFormData.date_published = moment(formattedFormData.date_published, dateFormat);
  }

  const onFinish = values => {
    setError(null);
    setIsFormSubmitting(true);
    const data = {...values};
    if (data.hasOwnProperty('date_published')) {
      data.date_published = data.date_published.format('YYYY-MM-DD');
    }
    axios({
      method,
      url,
      data
    })
    .then(({data}) => {
      if (data.success) {
        // Purposely delay
        setTimeout(() => {
          closeDrawer(method == 'post' ? {sortField: "id", sortOrder: "descend"} : true);
          setIsFormSubmitting(false);
        }, 1000);
      }
    })
    .catch(({response}) => {
      const res = response.data;
      if (res) {
        const list = [];
        for(const key in res.errors) {
          list.push({
            field: key,
            messages: res.errors[key].join(' ')
          });
        }
        setError({
          title: res.message,
          list: list
        });
      }
      setIsFormSubmitting(false);
    });
  };

  return (
      <div>
          <Form layout="vertical" hideRequiredMark onFinish={onFinish} ref={formRef} initialValues={formattedFormData}>
          {error ? 
          <Row>
            <Col>
              <Alert
                message={<b>{error.title}</b>}
                description={error.list.map((err) => <div key={err.field}>{err.messages}</div>)}
                type="error"
                style={{
                  marginBottom: "12px"
                }}
              />
            </Col>
          </Row>
          : null}
          <Row>
            <Col>
              <Form.Item
                name="title"
                label="Title"
                rules={[{ required: true, message: 'Please enter a title' }]}
              >
                <Input placeholder="Please enter a title" maxLength={25} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Item
                name="date_published"
                label="Date published"
                rules={[{ required: true, message: 'Please enter the date published' }]}
              >
                <DatePicker
                  format={dateFormat}
                  disabledDate={(currentDate) => {
                    return currentDate.diff(new Date, 'days') >= 0;
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Item
                name="author_id"
                label="Author"
                rules={[{ required: true, message: 'Please choose the Author' }]}
              >
                <Select placeholder="Please choose the Author">
                  {authors.map(author => <Select.Option key={author.id} value={author.id}>{`${author.first_name} ${author.last_name}`}</Select.Option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Item
                name="isbn"
                label="ISBN (13)"
                rules={[{ required: true, message: 'Please enter the ISBN' }]}
              >
                <NumericInput
                  value={formattedFormData ? formattedFormData.isbn : null}
                  placeholder="Please enter the ISBN"
                  style={{ width: 120 }}
                  maxLength={13}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
  )
}

export default BookForm
