import React, { useState } from 'react'
import { Alert, Form, Col, Row, Input } from 'antd';
import axios from "../../axios";
import { ConsoleSqlOutlined } from '@ant-design/icons';

const AuthorForm = ({
  formRef,
  closeDrawer,
  setIsFormSubmitting,
  formConfig: {
    editData = {
      id: null,
      first_name: null,
      last_name: null,
    },
    url,
    method
  },
}) => {
  const [error, setError] = useState(null);
  const onFinish = values => {
    setError(null);
    setIsFormSubmitting(true);
    axios({
      method,
      url,
      data: values
    })
    .then(({data}) => {
      if (data.success) {
        // Purposely delay
        setTimeout(() => {
          closeDrawer(method == 'post' ? {sortField: "id", sortOrder: "descend"} : true);
          setIsFormSubmitting(false);
        }, 1000);
      }
    })
    .catch(({response}) => {
      const res = response.data;
      if (res) {
        const list = [];
        if (res.errors) {
          for(const key in res.errors) {
            list.push({
              field: key,
              messages: res.errors[key].join(' ')
            });
          }
        }
        setError({
          title: res.message,
          list: list
        });
      }
      setIsFormSubmitting(false);
    });
  };

  return (
      <div>
          <Form layout="vertical" hideRequiredMark onFinish={onFinish} ref={formRef} initialValues={editData}>
          {error ? 
          <Row>
            <Col>
              <Alert
                message={<b>{error.title}</b>}
                description={error.list.map((err) => <div key={err.field}>{err.messages}</div>)}
                type="error"
                style={{
                  marginBottom: "12px"
                }}
              />
            </Col>
          </Row>
          : null}
          <Row>
            <Col>
              <Form.Item
                name="first_name"
                label="First Name"
                rules={[{ required: true, message: 'Please enter a title' }]}
              >
                <Input placeholder="Please enter a title" maxLength={25} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
            <Form.Item
                name="last_name"
                label="Last Name"
                rules={[{ required: true, message: 'Please enter a title' }]}
              >
                <Input placeholder="Please enter a title" maxLength={25} />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
  )
}

export default AuthorForm
