<?php

// resources/lang/en/messages.php

return [
    'general' => [
        'error'     => "Something's wrong with the platform, please contact the web admin.",
        'meh'       => [
            'deletion'  => "There's no record for id ':id', it might already been deleted."
        ]
    ],
    'author' => [
        'validation'    => [
            'already_exists'  => 'The author :full_name already exists.',
        ],
        'store' => [
            'success'   => 'You have added a new author successfully!',
        ],
        'update' => [
            'success'   => "You have updated :full_name's info successfully!",
        ],
        'destroy' => [
            'warning'   => "If an author is deleted it will be gone along with all his/her books.",
            'success'   => "You have deleted :full_name's record successfully along with all his/her books",
        ],
    ],
    'book' => [
        'validation'    => [
            'unique_title'  => 'The :attribute together with the date published must be unique.',
        ],
        'store'         => [
            'success'       => 'You have added a book successfully!',
        ],
        'update'        => [
            'success'       => "You have updated ':title' book successfully!",
        ],
        'destroy'       => [
            'success'       => "You have deleted ':title' book successfully!",
        ],
    ],
];