## Instructions

1. git clone https://ealvizouri@bitbucket.org/alvizouri1/materializelabs.git
2. docker-compose build && docker-compose up
3. docker-compose exec php composer install
4. cd src
5. copy .env.example .env
6. In .env file change database settings to:
   DB_HOST=mysql
   DB_DATABASE=bookshelf
   DB_USERNAME=root
   DB_PASSWORD=root
7. docker-compose exec php php artisan key:generate
8. docker-compose exec php php artisan migrate
9. npm install && npm run dev
